package com.tsvetomir.paymentplatform.account.service

import com.tsvetomir.paymentplatform.account.dto.request.CreateAccountRequestDTO
import com.tsvetomir.paymentplatform.account.dto.response.AccountResponseDTO

interface AccountService {

    fun create(criteria: CreateAccountRequestDTO): AccountResponseDTO

    fun getById(id: Long): AccountResponseDTO
}