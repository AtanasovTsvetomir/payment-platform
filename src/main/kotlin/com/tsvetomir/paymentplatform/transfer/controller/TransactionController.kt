package com.tsvetomir.paymentplatform.transfer.controller

import com.tsvetomir.paymentplatform.transfer.dto.request.CreateTransactionRequestDTO
import com.tsvetomir.paymentplatform.transfer.dto.response.TransactionResponseDTO
import com.tsvetomir.paymentplatform.transfer.service.TransactionService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/transaction")
class TransactionController(
    private val transactionService: TransactionService
) {

    @PostMapping
    fun createTransaction(@RequestBody criteria: CreateTransactionRequestDTO): TransactionResponseDTO {
        return this.transactionService.create(criteria)
    }
}