package com.tsvetomir.paymentplatform.utils.exception

import com.tsvetomir.paymentplatform.utils.Constants
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

class CustomExceptions {

    class AccountNotFoundException :
        ResponseStatusException(HttpStatus.NOT_FOUND, Constants.ACCOUNT_NOT_FOUND)

    class AccountCannotBeNegativeException :
        ResponseStatusException(HttpStatus.FORBIDDEN, Constants.ACCOUNT_BALANCE_CANNOT_BE_NEGATIVE)

    class RequestedAmountCannotBeBiggerThanBalanceException :
        ResponseStatusException(HttpStatus.FORBIDDEN, Constants.REQUESTED_AMOUNT_CANNOT_BE_BIGGER_THAN_BALANCE)
}