import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.util.*

val liquibaseVersion by extra("4.19.0")
val postgreVersion by extra("42.2.17")

plugins {
    id("org.springframework.boot") version "3.2.4"
    id("io.spring.dependency-management") version "1.1.4"
    id("org.liquibase.gradle") version "2.0.4"
    kotlin("jvm") version "1.9.23"
    kotlin("plugin.spring") version "1.9.23"
    kotlin("plugin.jpa") version "1.9.23"
}

group = "com.tsvetomir"
version = "0.0.1-SNAPSHOT"

java {
    sourceCompatibility = JavaVersion.VERSION_21
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.liquibase:liquibase-core")
    implementation("org.postgresql", "postgresql", postgreVersion)
    implementation("org.springdoc:springdoc-openapi-starter-webmvc-ui:2.0.2")
    implementation("org.springframework.boot:spring-boot-starter-validation")

    liquibaseRuntime("org.liquibase:liquibase-core:$liquibaseVersion")
    liquibaseRuntime("org.postgresql", "postgresql", postgreVersion)
    liquibaseRuntime("org.liquibase.ext", "liquibase-hibernate5", liquibaseVersion)
    liquibaseRuntime("org.springframework.boot:spring-boot:3.2.4")
    liquibaseRuntime("org.springframework:spring-orm")
    liquibaseRuntime("org.jetbrains.kotlin:kotlin-reflect")
    liquibaseRuntime("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    liquibaseRuntime(sourceSets.main.get().output)

    runtimeOnly("org.postgresql:postgresql")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
}

liquibase {
    activities.register("main") {
        val props = Properties()
        val fis = File(project.rootDir.absolutePath, "src/main/resources/application-local.properties").inputStream()
        props.load(fis)
        this.arguments = mapOf(
            "logLevel" to "info",
            "changeLogFile" to "src/main/resources/db/changelog/changes/changelog-temp.xml",
            "url" to props["spring.datasource.url"],
            "username" to props["spring.datasource.username"],
            "password" to props["spring.datasource.password"],
            "driver" to "org.postgresql.Driver",
            "referenceDriver" to "liquibase.ext.hibernate.database.connection.HibernateDriver",
            "referenceUrl" to "hibernate:spring:com.appolica.**?dialect=org.hibernate.dialect.PostgreSQL9Dialect&hibernate.physical_naming_strategy=org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy&hibernate.implicit_naming_strategy=org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy"
        )
    }
    runList = "main"
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs += "-Xjsr305=strict"
        jvmTarget = "21"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
