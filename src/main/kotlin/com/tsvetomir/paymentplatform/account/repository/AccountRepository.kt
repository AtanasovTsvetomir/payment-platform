package com.tsvetomir.paymentplatform.account.repository

import com.tsvetomir.paymentplatform.account.domain.Account
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface AccountRepository : JpaRepository<Account, Long>