package com.tsvetomir.paymentplatform.account.controller

import com.tsvetomir.paymentplatform.account.dto.request.CreateAccountRequestDTO
import com.tsvetomir.paymentplatform.account.dto.response.AccountResponseDTO
import com.tsvetomir.paymentplatform.account.service.AccountService
import com.tsvetomir.paymentplatform.transfer.dto.response.GetAllTransactionsResponseDTO
import com.tsvetomir.paymentplatform.transfer.service.TransactionService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/account")
class AccountController(
    private val moneyAccountService: AccountService,
    private val transactionService: TransactionService
) {

    @PostMapping
    fun createAccount(@RequestBody criteria: CreateAccountRequestDTO): AccountResponseDTO {
        return this.moneyAccountService.create(criteria)
    }

    @GetMapping("/{accountId}")
    fun getAccount(@PathVariable accountId: Long): AccountResponseDTO {
        return this.moneyAccountService.getById(accountId)
    }

    @GetMapping("/{accountId}/transactions")
    fun getTransactionsByAccountId(@PathVariable accountId: Long): List<GetAllTransactionsResponseDTO> {
        return this.transactionService.getByAccountId(accountId)
    }
}