package com.tsvetomir.paymentplatform.transfer.service

import com.tsvetomir.paymentplatform.transfer.dto.request.CreateTransactionRequestDTO
import com.tsvetomir.paymentplatform.transfer.dto.response.GetAllTransactionsResponseDTO
import com.tsvetomir.paymentplatform.transfer.dto.response.TransactionResponseDTO

interface TransactionService {

    fun create(criteria: CreateTransactionRequestDTO): TransactionResponseDTO

    fun getByAccountId(accountId: Long): List<GetAllTransactionsResponseDTO>
}