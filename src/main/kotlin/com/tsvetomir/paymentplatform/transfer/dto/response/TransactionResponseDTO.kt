package com.tsvetomir.paymentplatform.transfer.dto.response

import com.tsvetomir.paymentplatform.transfer.domain.TransactionType
import java.math.BigDecimal
import java.time.Instant

data class TransactionResponseDTO(
    val transferId: Long,
    val accountId: Long,
    val transactionType: TransactionType,
    val amount: BigDecimal,
    val date: Instant,
)
