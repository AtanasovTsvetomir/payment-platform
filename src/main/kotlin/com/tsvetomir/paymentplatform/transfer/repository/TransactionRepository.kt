package com.tsvetomir.paymentplatform.transfer.repository

import com.tsvetomir.paymentplatform.transfer.domain.Transaction
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface TransactionRepository : JpaRepository<Transaction, Long> {

    fun findAllByAccountId(accountId: Long): List<Transaction>
}