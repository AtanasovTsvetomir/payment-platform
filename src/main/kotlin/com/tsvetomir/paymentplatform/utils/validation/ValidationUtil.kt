package com.tsvetomir.paymentplatform.utils.validation

import com.tsvetomir.paymentplatform.utils.exception.CustomExceptions
import java.math.BigDecimal

class ValidationUtil {

    companion object {
        fun validateInitialBalance(balance: BigDecimal) {
            if (balance.compareTo(BigDecimal.ZERO) == -1) {
                throw CustomExceptions.AccountCannotBeNegativeException()
            }
        }
    }
}