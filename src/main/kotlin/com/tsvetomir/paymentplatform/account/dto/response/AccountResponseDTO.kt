package com.tsvetomir.paymentplatform.account.dto.response

import java.math.BigDecimal

data class AccountResponseDTO(
    val accountId: Long,
    val name: String,
    val currentBalance: BigDecimal
)
