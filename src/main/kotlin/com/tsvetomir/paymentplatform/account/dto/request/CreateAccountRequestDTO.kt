package com.tsvetomir.paymentplatform.account.dto.request

import java.math.BigDecimal

data class CreateAccountRequestDTO(
    val name: String,
    val initialBalance: BigDecimal
)
