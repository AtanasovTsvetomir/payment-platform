package com.tsvetomir.paymentplatform.account.service.impl

import com.tsvetomir.paymentplatform.account.domain.Account
import com.tsvetomir.paymentplatform.account.dto.request.CreateAccountRequestDTO
import com.tsvetomir.paymentplatform.account.dto.response.AccountResponseDTO
import com.tsvetomir.paymentplatform.account.repository.AccountRepository
import com.tsvetomir.paymentplatform.account.service.AccountService
import com.tsvetomir.paymentplatform.utils.exception.CustomExceptions
import com.tsvetomir.paymentplatform.utils.validation.ValidationUtil
import org.springframework.stereotype.Service

@Service
class AccountServiceImpl(
    private val accountRepository: AccountRepository
) : AccountService {

    override fun create(criteria: CreateAccountRequestDTO): AccountResponseDTO {
        ValidationUtil.validateInitialBalance(criteria.initialBalance)

        val account = Account(criteria.name, criteria.initialBalance)
        this.accountRepository.save(account).let {
            return AccountResponseDTO(
                accountId = it.id,
                name = it.name,
                currentBalance = it.balance
            )
        }
    }

    override fun getById(id: Long): AccountResponseDTO {
        val account = this.accountRepository.findById(id).orElseThrow { CustomExceptions.AccountNotFoundException() }

        return AccountResponseDTO(
            accountId = account.id,
            name = account.name,
            currentBalance = account.balance
        )
    }

}