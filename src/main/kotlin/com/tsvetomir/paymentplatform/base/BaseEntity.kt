package com.tsvetomir.paymentplatform.base

import jakarta.persistence.*
import java.time.Instant

@MappedSuperclass
open class BaseEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    open val id: Long = 0,

    @Column(name = "time_created")
    open var timeCreated: Instant = Instant.now()
)