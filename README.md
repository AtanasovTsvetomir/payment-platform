# Payment Platform

I have used:
1. Docker for PostgreSQL database
2. Liquibase for database migrations
3. Swagger for documentation and testing

Use swagger to test the platform:

http://localhost:8080/swagger-ui/index.html#/

# Prerequisites
1. Java 21
2. Docker compose

## Getting started

1. Go to package /local
2. Run docker-compose up
3. Run the project