package com.tsvetomir.paymentplatform.transfer.dto.request

import com.tsvetomir.paymentplatform.transfer.domain.TransactionType
import java.math.BigDecimal

data class CreateTransactionRequestDTO(
    val accountId: Long,
    val transactionType: TransactionType,
    val amount: BigDecimal
)
