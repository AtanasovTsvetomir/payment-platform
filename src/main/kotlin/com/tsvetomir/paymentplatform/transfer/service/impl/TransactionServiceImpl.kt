package com.tsvetomir.paymentplatform.transfer.service.impl

import com.tsvetomir.paymentplatform.account.repository.AccountRepository
import com.tsvetomir.paymentplatform.transfer.domain.Transaction
import com.tsvetomir.paymentplatform.transfer.domain.TransactionType
import com.tsvetomir.paymentplatform.transfer.dto.request.CreateTransactionRequestDTO
import com.tsvetomir.paymentplatform.transfer.dto.response.GetAllTransactionsResponseDTO
import com.tsvetomir.paymentplatform.transfer.dto.response.TransactionResponseDTO
import com.tsvetomir.paymentplatform.transfer.repository.TransactionRepository
import com.tsvetomir.paymentplatform.transfer.service.TransactionService
import com.tsvetomir.paymentplatform.utils.exception.CustomExceptions
import com.tsvetomir.paymentplatform.utils.validation.ValidationUtil
import org.springframework.stereotype.Service

@Service
class TransactionServiceImpl(
    private val accountRepository: AccountRepository,
    private val transactionRepository: TransactionRepository
) : TransactionService {

    override fun create(criteria: CreateTransactionRequestDTO): TransactionResponseDTO {
        ValidationUtil.validateInitialBalance(criteria.amount)

        val account = this.accountRepository
            .findById(criteria.accountId)
            .orElseThrow { CustomExceptions.AccountNotFoundException() }

        if (criteria.transactionType === TransactionType.WITHDRAW && account.balance.compareTo(criteria.amount) == -1) {
            throw CustomExceptions.RequestedAmountCannotBeBiggerThanBalanceException()
        }

        try {
            when (criteria.transactionType) {
                TransactionType.DEPOSIT -> {
                    account.balance += criteria.amount
                }

                TransactionType.WITHDRAW -> {
                    account.balance -= criteria.amount
                }
            }

            this.accountRepository.save(account)
            this.transactionRepository.save(Transaction(
                transactionType = criteria.transactionType,
                amount = criteria.amount,
                account = account
            )).let {
                return TransactionResponseDTO(
                    transferId = it.id,
                    accountId = it.account.id,
                    transactionType = it.transactionType,
                    amount = it.amount,
                    date = it.timeCreated
                )
            }
        } catch (e: Exception) {
            throw Exception("Something went wrong")
        }
    }

    override fun getByAccountId(accountId: Long): List<GetAllTransactionsResponseDTO> {
        return this.transactionRepository.findAllByAccountId(accountId)
            .map { transaction ->
                GetAllTransactionsResponseDTO(
                    transferId = transaction.id,
                    transactionType = transaction.transactionType,
                    amount = transaction.amount,
                    date = transaction.timeCreated
                )
            }
    }
}