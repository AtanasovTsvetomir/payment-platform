package com.tsvetomir.paymentplatform.transfer.domain

import com.tsvetomir.paymentplatform.account.domain.Account
import com.tsvetomir.paymentplatform.base.BaseEntity
import jakarta.persistence.*
import java.math.BigDecimal

@Entity
@Table(name = "transaction", schema = "public")
data class Transaction(
    @Column(name = "transaction_type")
    @Enumerated(EnumType.STRING)
    val transactionType: TransactionType,

    @Column(name = "amount", precision = 12, scale = 6)
    val amount: BigDecimal,

    @ManyToOne
    val account: Account
) : BaseEntity()

enum class TransactionType {
    DEPOSIT, WITHDRAW
}