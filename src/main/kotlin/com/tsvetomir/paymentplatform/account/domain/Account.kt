package com.tsvetomir.paymentplatform.account.domain

import com.tsvetomir.paymentplatform.base.BaseEntity
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Table
import java.math.BigDecimal

@Entity
@Table(name = "account", schema = "public")
data class Account(
    @Column(name = "name")
    val name: String,

    @Column(name = "balance", precision = 12, scale = 6)
    var balance: BigDecimal
) : BaseEntity()