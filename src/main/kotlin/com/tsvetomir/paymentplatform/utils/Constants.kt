package com.tsvetomir.paymentplatform.utils

class Constants {

    companion object {
        const val ACCOUNT_NOT_FOUND = "ACCOUNT_NOT_FOUND"
        const val ACCOUNT_BALANCE_CANNOT_BE_NEGATIVE = "ACCOUNT_BALANCE_CANNOT_BE_NEGATIVE"
        const val REQUESTED_AMOUNT_CANNOT_BE_BIGGER_THAN_BALANCE = "REQUESTED_AMOUNT_CANNOT_BE_BIGGER_THAN_BALANCE"
    }
}