package com.tsvetomir.paymentplatform.transfer.dto.response

import com.tsvetomir.paymentplatform.transfer.domain.TransactionType
import java.math.BigDecimal
import java.time.Instant

data class GetAllTransactionsResponseDTO(
    val transferId: Long,
    val transactionType: TransactionType,
    val amount: BigDecimal,
    val date: Instant,
)
